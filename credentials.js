module.exports = {
  cookieSecret: 'cookiemonster',
  mongo: {
    development: {
        connectionString: 'mongodb://blogableuser:blogablepass@ds119608.mlab.com:19608/blogable'
    },
    production: {
        connectionString: 'mongodb://blogableuser:blogablepass@ds119608.mlab.com:19608/blogable'
    }
  }
}
