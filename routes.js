
/**
* lots of help obtained from mongoose documentation
**/

var passport = require('passport');
//var User = require("./models/user.js");
var Account = require("./models/account.js");
var router = require('express').Router();
var apiKey = require("./models/apiKey.js");
var admin = require("./models/admin.js");
var post = require("./models/post.js");
var connectionManager = require("./lib/MYSQLDBConnectionManager.js");

router.get('/', function(req, res) {
  post.find({},null,{sort: '-postDate'},function(err, posts) {
    if (err) console.log(err);
    else res.render('index', {
      user: req.user,
      posts: posts
    });

  });
    //res.render('index', {user: req.user});
});
router.get('/api/recipebook/KT', function(req,res){
  res.render('kt');
});
router.get('/api/recipebook/keys/generate/:type/:num', function(req,res){
  //TODO Add code to randomly generate and authorize codes or call upon java app to do so.
});
router.get('/api/recipebook/resolve', function(req,res){
  console.log(req.query);
  if (req.query.UID == 5){
    res.send("Correct");
  } else res.send("Incorrect");
});

router.get('/blog/search', function(req, res){
  var query = {
    title: new RegExp(req.query.search, 'i')
  }
  post.find(query,null,{sort: '-postDate'},function(err, posts) {
    if (err) console.log(err);
    else res.render('index', {
      user: req.user,
      posts: posts
    });
  });
});
router.get('/register', function(req, res) {
  res.render('register', {});
});

router.post('/register', function(req,res, next){
  console.log('registering user');
  Account.register(new Account({username: req.body.username}), req.body.password, function(err) {
    if (err) {
      console.log('error while user register!', err);
      return next(err);
    }
    console.log('user registered!');
    res.redirect('/');
  });
});

router.get('/login', function(req, res) {
  res.render('login', {user: req.user});
});

router.post('/login', passport.authenticate('local'), function(req, res) {
  res.redirect('/blog');
});

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});
router.get('/api/decibelatlas/insert/:decibels/:latitude/:longitude/:altitude/:accuracy/:time', function(req, res){
  console.log("Route Sent");
  connectionManager.connect();
  connectionManager.query('INSERT INTO DADB(decibels,latitude,longitude,altitude,accuracy,recordtime) values ('+req.params.decibels+ ','+req.params.latitude+ ','+ req.params.longitude+ ','+ req.params.altitude+ ','+req.params.accuracy  + ','  + req.params.time  + ');', function(err, rows, fields) {
    console.log("Query Finished");
    if (!err){
      res.json({status: "Success"});
      //connectionManager.disconnect();
    }
    else {
      res.json({status: "Failure"});
      //connectionManager.disconnect();
    }
  });
});
router.get('/api/decibelatlas/get/:latitude/:longitude/:altitude/:time', function(req,res){
  var lat = Number(req.params.latitude);
  var long = Number(req.params.longitude);
  var latLowerBound = lat-1;
  var latUpperBound = lat+1;
  var longLowerBound = long-1;
  var longUpperBound = long+1;
  connectionManager.connect();
  connectionManager.query('SELECT decibels,latitude,longitude,altitude,accuracy,recordtime FROM DADB WHERE latitude BETWEEN ' + latLowerBound + ' AND ' + latUpperBound + ' AND longitude BETWEEN ' + longLowerBound + ' AND ' + longUpperBound + ';',function(err,rows,fields){
    console.log("Query Finished");
    var testString = "Test: " + rows.length;
    if(!err){
      var array = [];
      for (var i = 0; i < rows.length; i++){
        array[i] = {
          decibels: rows[i]["decibels"],
          latitude: rows[i]["latitude"],
          longitude: rows[i]["longitude"],
          altitude: rows[i]["altitude"],
          accuracy: rows[i]["accuracy"],
          time: rows[i]["recordtime"]
        }
      }
      console.log(rows.length);
      console.log(fields);
      res.json({
        data: array
      });
    }
  });
});

router.get('/api/master/override/apimaster/verified/add/:key', function(req, res){
  var keyCur = new apiKey({
    key: req.params.key
  });
  keyCur.save(function(err, data){
    if (err) console.log(err);
    else console.log('Saved : ', data );
  });
  res.redirect(303, '/');
});

router.get('/blog/add', function(req, res){
  res.render('createPost', {user: req.user});
});
router.post('/blog/add', function(req, res){
  if (req.user){
    var title = req.body.title;
    var author = req.user.username;
    var content =  req.body.content;
    var timestamp = new Date();
    var timeHours = timestamp.getHours();
    var timeHoursText = "";
    if (timeHours < 10) timeHoursText = "0" + timeHours;
    else timeHoursText = "" + timeHours;
    var timeMinutes = timestamp.getMinutes();
    var timeMinutesText = "";
    if (timeMinutes < 10) timeMinutesText = "0" + timeMinutes;
    else timeMinutesText = ""+timeMinutes;
    var timeSeconds = timestamp.getSeconds();
    var timeSecondsText = "";
    if (timeSeconds < 10) timeSecondsText = "0" + timeSeconds;
    else timeSecondsText = "" + timeSeconds;
    var timeMonths = timestamp.getMonth();
    var timeMonthsText = "";
    if (timeMonths < 10) timeMonthsText = "0" + timeMonths;
    else timeMonthsText = "" + timeMonths;
    var timeDays = timestamp.getDate();
    var timeDaysText = "";
    if (timeDays < 10) timeDaysText = "0" + timeDays;
    else timeDaysText = "" + timeDays;
    var postDate = "" + timeMonthsText + "-" + timeDaysText + "-" + timestamp.getFullYear() + ", " + timeHoursText + ":" + timeMinutesText + ":" + timeSecondsText;
    var imageUrl = req.body.imageUrl;
    var postToPost = new post({
      title: title,
      author: author,
      content: content,
      postDate: postDate,
      imageUrl: imageUrl
    });
    postToPost.save(function(err, data){
      if (err) console.log(err);
      else console.log('Saved : ', data );
    });
    res.redirect(303, '/blog');
  } else{
    res.redirect(403, "/login");
  }
});
router.get('/blog/admin/:user', function(req,res){
  post.find({author: req.params.user},null,{sort: '-postDate'},function(err, posts) {
    if(err) console.log(err);
    else if(admin.findOne({uname: req.user.username})){
      res.render('dashboard', {
        user: req.user,
        posts: posts
      });
    } else res.redirect(404, "/404");
  });
});
router.get('/blog/', function(req, res){
  post.find({author: req.user.username},null,{sort: '-postDate'},function(err, posts) {
    if (err) console.log(err);
    else res.render('dashboard', {
      user: req.user,
      posts: posts
    });
  });
});
router.post('/blog/edit/:user/:post', function(req, res){
  if (req.user.username == req.params.user){
    var title = req.body.title;
    var content = req.body.content;
    var imageUrl = req.body.imageUrl;
    var postToPost = {
      title: title,
      content: content,
      imageUrl: imageUrl
    };
    console.log(postToPost);
    post.findOne({author: req.user.username, title: req.params.post},function(err, posts){
      console.log(posts);
    });
    post.findOneAndUpdate({author: req.user.username, title: req.params.post}, postToPost, {}, function(err, data){
      if(err) console.log(err);
      else console.log('Saved : ', data );
    });
    res.redirect(303, "/blog/" + req.params.user + "/" + req.body.title);
  } else{
    res.redirect(403, "/blog/" + req.params.user + "/" + req.params.post);
  }
});
router.get('/blog/edit/:user/:post', function(req,res){
  var userQ = req.params.user;
  var titleQ = req.params.post;
  post.find({author: userQ, title: titleQ}, function(err, posts){
    var itemList = posts.map(function(item){
      return {
        title: item.title,
        author: item.author,
        content: item.content,
        postDate: item.postDate,
        imageUrl: item.imageUrl
      }
    });
    console.log(itemList);
    var authorized = false;
    if (req.user.username == itemList[0].author){
      authorized = true;
    }
    if(authorized){
      res.render('editPost', {
        user: req.user,
        authorized: authorized,
        title: itemList[0].title,
        author: itemList[0].author,
        content: itemList[0].content,
        postDate: itemList[0].postDate,
        imageUrl: itemList[0].imageUrl
      });
    } else{
      res.render('post', {
        user: req.user,
        authorized: false,
        title: "Unauthorized",
        author: "Unauthorized",
        content: "Unauthorized",
        postDate: "Unauthorized",
        imageUrl: "Unauthorized"
      });
    }
  });
});
router.get('/blog/admin', function(req,res){
  Account.find({},function(err, posts) {
    if (err) console.log(err)
    else{
      admin.find({uname: req.user.username}, function(err, users) {
        if (err) console.log(err)
        else{
          console.log(users);
          if (users.length > 0){
            console.log(posts);
            res.render('adminDashboard', {
              user: req.user,
              posts: posts
            });
          }else res.redirect(403, '/blog');
        }
      });
    }
  });
});
router.get('/blog/:user/:post', function(req, res){
  var userQ = req.params.user;
  var titleQ = req.params.post;
  console.log(titleQ);
  var itemList;
  post.findOne({author: userQ, title: titleQ}, function(err, posts){
    console.log(posts);
    var authorized = false;
    console.log(req.user.username);

    if (posts){
      if (req.user.username == posts.author){
        authorized = true;
      }
      res.render('post', {
        user: req.user,
        authorized: authorized,
        title: posts.title,
        author: posts.author,
        content: posts.content,
        postDate: posts.postDate,
        imageUrl: posts.imageUrl
      });
    } else{
      res.redirect(404, "/404");
    }
  });

});
router.get('/api/:key/upgrade/:user', function(req, res){
  var key = req.params.key;
  var user = req.params.user;
  admin.find({key: key}, function(err, keys){
    if(keys.length){
      console.log('invalid api key');
      res.redirect(303, '/');
    }
  });
  admin.find({uname: user}, function(err, admins){
    console.log('admins-matched: ' + admins.length);
    if (admins.length == 0){
      var auser = new admin({uname: user});
      auser.save(function(err, data){
        if (err) console.log(err);
        else console.log('Saved : ', data );
      });
      console.log('success');
    }
  });
  res.redirect(303, '/');
});


















module.exports = router;
